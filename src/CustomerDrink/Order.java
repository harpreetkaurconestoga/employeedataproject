package CustomerDrink;
import java.util.Arrays;

public class Order {

    String drinkType = "";
    String drinkSelection = "";
    String drinkSize = "";
    String Name = "";
    String FlavorAdded = "";
    Double Cost=0.0;
    Double CostWithTax=0.0;
    Double SmallSize=1.5;
    Double MediumSize=2.5;
    Double LargeSize=3.5;
    Double Vanilla=0.25;
    Double Chacolate=0.75;
    Double Lemon=0.25;
    Double Mint=0.50;
    Double TaxRate=1.13;

    String[] TeaValidStrings = {"tea", "t"};
    String[] CoffeeValidStrings = {"coffee", "c"};

    String[] validSizes = {"s", "small", "m", "medium", "l", "large"};
    String[] TeaFlavors = {"none", "n", "mint", "m", "lemon", "l"};
    String[] CoffeeFlavors = {"none", "n", "vanilla", "v", "chocolate", "c"};

    public boolean isBevTypeValid() {
        if (Arrays.asList(this.TeaValidStrings).contains(this.drinkSelection.toLowerCase())) {
            this.drinkType = "Tea";
            return true;
        } else if (Arrays.asList(this.CoffeeValidStrings).contains(this.drinkSelection.toLowerCase())) {
            this.drinkType = "Coffee";
            return true;
        } else
            return false;
    }

    public boolean isvalidSize() {
        if (Arrays.asList(this.validSizes).contains(this.drinkSize.toLowerCase())) {
            switch (this.drinkSize.toLowerCase()) {
                case "s":
                case "small":
                    this.drinkSize = "Small";
                    this.Cost = this.Cost+this.SmallSize;
                    break;
                case "m":
                case "medium":
                    this.drinkSize = "Medium";
                    this.Cost = this.Cost + this.MediumSize;
                    break;
                case "l":
                case "large":
                    this.drinkSize = "Large";
                    this.Cost = this.Cost+ this.LargeSize;
                    break;
            }
            return true;
        } else
            return false;
    }

    public boolean isvalidTeaFlavor() {
        if (Arrays.asList(this.TeaFlavors).contains(this.FlavorAdded.toLowerCase())) {
            switch (this.FlavorAdded.toLowerCase()) {
                case "n":
                    this.FlavorAdded = "none";
                    break;
                case "m":
                    this.FlavorAdded = "mint";
                    this.Cost = this.Cost+this.Mint;
                    break;
                case "l":
                    this.FlavorAdded = "lemon";
                    this.Cost = this.Cost+this.Lemon;
                    break;
            }
            return true;
        } else
            return false;
    }

    public boolean isvalidCoffeeFlavor() {
        if (Arrays.asList(this.CoffeeFlavors).contains(this.FlavorAdded.toLowerCase())) {
            switch (this.FlavorAdded.toLowerCase()) {
                case "n":
                    this.FlavorAdded = "none";
                    break;
                case "v":
                    this.FlavorAdded = "vanilla";
                    this.Cost = this.Cost+this.Vanilla;
                    break;
                case "c":
                    this.FlavorAdded = "chocolate";
                    this.Cost = this.Cost+this.Chacolate;
                    break;
            }
            return true;
        } else
            return false;
    }
}
