package CustomerDrink;
import javax.swing.*;



public class main {

    public static void main(String[] args) {

        Order Order1 = new Order();
        Order1.Name = JOptionPane.showInputDialog("Enter your name");

        Order1.drinkSelection = JOptionPane.showInputDialog("Select Drink. Tea or Coffee");

        if (!Order1.isBevTypeValid()) {
            JOptionPane.showMessageDialog(null, Order1.drinkSelection + " is not a valid drink selection");
            System.exit(0);
        }

        Order1.drinkSize = JOptionPane.showInputDialog("Please select drink size: Small, Medium, Large");

        if (!Order1.isvalidSize()) {
            JOptionPane.showMessageDialog(null, Order1.drinkSize + " is not a valid size");

            System.exit(0);
        }
        if (Order1.drinkType == "Tea") {
            Order1.FlavorAdded = JOptionPane.showInputDialog("Please select flavor none, mint, lemon");
            if (!Order1.isvalidTeaFlavor()) {


                JOptionPane.showMessageDialog(null, Order1.FlavorAdded + " is not a valid for tea");

                System.exit(0);
            }
        } else if (Order1.drinkType == "Coffee"){
            // if coffee selected
            Order1.FlavorAdded = JOptionPane.showInputDialog("Select flavor none, vanilla, chocolate");

            if (!Order1.isvalidCoffeeFlavor()) {


                JOptionPane.showMessageDialog(null, Order1.FlavorAdded + " is not a valid for coffee");
                // Exit if selection is not valid
                System.exit(0);
            }
        }

        Order1.CostWithTax=Order1.Cost*Order1.TaxRate;
        Double TotalCostRounded= (double) Math.round( Order1.CostWithTax * 100) / 100;
        JOptionPane.showMessageDialog(null, "For " + Order1.Name+"," + " a " + Order1.drinkSize +
                " " + Order1.drinkType + ", " + Order1.FlavorAdded + ", Cost:  $"+ TotalCostRounded);
    }
}








